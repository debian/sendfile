			 O-SAFT/fetchfile
			==================

With the server protocol extension O-SAFT (Offer Simple Asynchronous File
Transfer) and the matching client fetchfile there is an easy method of
retrieving files from a SAFT server. This is a direct analogy to the SMTP
and POP or APOP protocol suite in the world of e-mail transfer.

Overview:

	- How does O-SAFT/fetchfile work?

	- What to do on the client side?

	- What to do on the server side?

	- How about security issues?


		 How does O-SAFT/fetchfile work?
		---------------------------------

O-SAFT is an extension to the existing SAFT protocol and allows
athenticated clients to retrieve files from a (remote) server. The
implemention is the server sendfiled and the client fetchfile.

O-SAFT uses a dedicated pgp key pair to authenticate the fetchfile session.
The private key will be kept on the client side, the public key must tbe
present at the server side. For security reasons this will NOT be your
regular e-mail pgp key pair, but a separate pair of pgp keys, uniquely
assigned for fetchfile transfers. You will have to create a pair of pgp
keys for this purpose befor using the fetchfile client for the first time
(see below).

Fetchfile can provide a directory listing of available files from the server,
retrieve files or delete files. After retrieving a file, it will be placed
in the regular spool directory, not in the current directory! You will have
to use the receive command to transfer the files from the spool directory 
to your current directory afterwards.

If there already exists a regular sendfile spool directory /var/spool/sendfile
on the client side it will be used, otherwise a $HOME/.sfspool will be
created. Fetchfile will be running without using root permissions on the
client side.


		 What to do on the client side?
	        ---------------------------------

You must have pgp installed and the binaries must be available through your
$PATH environment variable.

First, and ONLY ONCE before using fetchfile the very first time, you
have to create a fetchfile pgp key pair:

   fetchfile -I

Please only hit 'ENTER' when being asked for a pass phrase! This will create
a special non-passphrase protected key pair for O-SAFT.

After this initialization you will have a file 
/var/spool/sendfile/$USER/config/public.pgp 	resp.
 $HOME/.sfspool/public.pgp

Please send this file to root@SAFT-server, who has to save this public key 
file into the appropiate user configuration directory.

Example:
sendfile -c "my O-SAFT puplic key" /var/spool/sendfile/$USER/config/public.pgp \
          root@bofh.belwue.de

(This prelimary action will enable you to use the SAFT server and will
prevent othes from abusing your name or SAFT-account on the server.)

After preparing the pgp keys an both sides, you can invoke fetchfile on
a regular basis:

   fetchfile -l             list files on the server
   fetchfile -a             retrieve all files from server
   fetchfile -daf *aol.com  delete all files from the AOL domain

There is a detailed description of all capabilities in the man page, which
can be read using 'man fetchfile'.

For configuring the server SAFT account there are two options:

   fetchfile -Cw=config
   fetchfile -Cw=restrictions

Using this the two local configuration files will be transfered from the
local current directory to the SAFT server. The details of the configuration
can be found with the 'man sendfile' command.


With using

   fetchfile -Cr=config
   fetchfile -Cr=restrictions

the files will be retrieved back and will be displayed to STDOUT.


		 What to do on the server side?
		---------------------------------

pgp must be installed. The system adminsitrator needs to run
sfdconf -e config and set the following lines:

	# allow O-SAFT extension (on/off)
	fetchfile = on

The system administrator must create a user account (if it does not yet
exist). This account does not need an interactive login shell and does
not need a valid password; the login shell could be /bin/false. The only
purpose is to enable the sendfiled to check out the user and to create
a local spool directory (this method is well known for creating POP mail
accounts).

The client user will create the initial pgp key pair and the public key
(public.pgp) will be sent to the system administrator of the server.
This key has to be placed into the config directory for the particular user.
Assuming the user name is bozo, the system administrator will have to
type the following (under root permissions):


	receive -f bozo@* -b bozo public.pgp
	su bozo
	cd /var/spool/sendfile/bozo/config
	receive public.pgp

(the first receive resends the file public.pgp from the sender bozo@* to the
 local user bozo)


		 How about security issues?
		------------------------------

O-SAFT uses a tcp challenge/response authentication with a pgp signature.
This opens the possibility that the session can be attacked through tcp
hijacking. We are well aware of this, but tcp hijacking is not easy and
only possible if the attacker has direct access to the transport media
(e.g. listening on the same ethernet cable/segment) and has access to a
set of pretty nice cracker tools. With regular operating system supplied
software it is not possible to attack a session.

translated by andreas@citecs.de, Thu Dec 18 13:50:31 PST 1997
