#!/bin/sh
#
# Das Schnellinstallations-Script fuer sendfile im /sw. 
# Bei Aufruf ohne Parameter wird ein Hilfetext ausgegeben.
#

mkdir_recursive() {
  if [ ! -d $1 ]; then
    mkdir `echo $1 | 
           awk -F/ '{ for (i=2; $i!=""; i++)
                      { for (j=2; j<=i; j++) printf("/"$j)
  		        printf(" ") } }'
          ` 2>/dev/null
  fi
}

RESTART=false
SPOOL=/var/spool/sendfile
CONFIG=/usr/local/etc
ALLOW=/usr/local/etc/sendfile.allow
DENY=/usr/local/etc/sendfile.deny
if [ "$2" = "" ]; then 
  SERVERDIR=/usr/local/sbin
else
  SERVERDIR=$2
fi

if [ "$1" != "ok" ]; then
  more <<EOD

Dies ist das no-questions-no-answers Schnellinstallations-Script fuer sendfile.
( Dokumentation zu SAFT/sendfile befindet sich in /client/doc/sendfile-2.1, bei
  Fragen oder Kritik bitte mail an framstag@rus.uni-stuttgart.de schicken. )

Es werden folgende Default-Werte benutzt (und automatisch angelegt):

	spool directory =	$SPOOL
	config directory =	$CONFIG
	sendfiled directory =	$SERVERDIR

In /etc/services und /etc/inetd.conf werden am Ende entsprechende Eintraege
gemacht.

In /etc/profile und in /etc/csh.login wird am Ende eine Zeile zum Test auf 
Files im sendfile-Spool hinzugefuegt.

Um den sendfile-Service wieder zu deinstallieren, genuegt ein:
	rm $SERVERDIR/sendfiled

Wollen Sie andere Default-Eintraege, so muessen Sie diese in makeconfig im
Source (ftp://ftp.uni-stuttgart.de/pub/unix/comm/misc/sendfile.tar.gz)
eintragen und selber compilieren. 

Einzige Ausnahme: das Directory des sendfiled kann auch hiermit angegeben
werden:
	/client/bin/sf_install ok /directory/wo/sendfiled/rein/soll

Sind Sie mit den Default-Eintraegen einverstanden, dann geben Sie jetzt ein:

	/client/bin/sf_install ok

EOD
  exit
fi

if [ "$LOGNAME" != root ]; then
  if [ "`whoami`" != root ]; then
    echo "Der SAFT/sendfile Service kann nur von root installiert werden!!"
    exit 1
  fi
fi

cd SW_INSTALL
umask 022

SENDFILED=`awk '/^saft/ { sfd=$6;
                          if (index($7,"/sendfiled")>0) sfd=$7;
                          if (substr(sfd,1,1)=="?") sfd=substr(sfd,2);
                          print sfd;
                        }' /etc/inetd.conf`
if [ "$SENDFILED" != "$SERVERDIR/sendfiled" -a "$SENDFILED" != "" ]; then
  echo "WARNING: cannot install sendfiled in $SERVERDIR because in /etc/inetd.conf"
  echo "         there is $SENDFILED specified! Check it!"
else
  echo "installing the sendfile-daemon in $SERVERDIR"
  mkdir_recursive $SERVERDIR
  cp sendfiled $SERVERDIR
  chmod 755 $SERVERDIR/sendfiled
fi

mkdir_recursive $CONFIG
NOSENDFILE=$CONFIG/nosendfile
if [ -f $NOSENDFILE ]; then 
  if grep ^allow-only $NOSENDFILE >/dev/null; then 
    echo "Information: $NOSENDFILE is now $ALLOW (autoconverted)"
    grep -v ^allow-only $NOSENDFILE > $ALLOW
  else  
    echo "Information: $NOSENDFILE is now $DENY (autoconverted)"
    mv $NOSENDFILE $DENY
  fi
fi
if [ ! -f $DENY -a ! -f $ALLOW]; then 
  echo "installing $DENY"
  cp sendfile.deny $DENY
  chmod 644 $DENY
fi

if [ ! -f $CONFIG/sendfile.cf ]; then 
  echo installing $CONFIG/sendfile.cf
  cp sendfile.cf $CONFIG/sendfile.cf
  chmod 644 $CONFIG/sendfile.cf
else
  if [ "`diff sendfile.cf $CONFIG`" ]; then
    echo "`pwd`/sendfile.cf differs from $CONFIG/sendfile.cf !"
    echo "Bitte von Hand nachschauen was sich geaendert hat!"
  fi
fi

SERVICES="`awk '/[ \t]487\/tcp/' /etc/services`"
if [ "$SERVICES" != "" ]; then
  case "$SERVICES" in 
    saft*)	;;
    *)		echo "install-error: tcp-port 487 ist bereits belegt!"; exit 1;;
  esac
else
  echo configuring /etc/services
  echo "#" >>/etc/services
  echo "saft	487/tcp		# simple asynchronous file transfer" >>/etc/services
fi

if [ "`grep '^saft' /etc/inetd.conf`" = "" ]; then
  if [ -f /usr/sbin/tcpd ]; then 
    SFD="/usr/sbin/tcpd $SERVERDIR/sendfiled"
  else
    SFD="$SERVERDIR/sendfiled sendfiled"
  fi
  RESTART=true
  echo configuring /etc/inetd.conf
  echo "#" >>/etc/inetd.conf
  echo "# simple asynchronous file transfer" >>/etc/inetd.conf
  echo "saft	stream	tcp	nowait	root	$SFD" >>/etc/inetd.conf
fi

if [ -f /etc/inetd.sec ]; then
  if [ "`grep '^saft' /etc/inetd.sec`" = "" ]; then
    echo >> /etc/inetd.sec
    echo "saft allow" >> /etc/inetd.sec
  fi
fi

if [ -f /etc/profile ]; then
  if [ "`grep check_sendfile /etc/profile`" = "" ]; then
    echo adding check_sendfile to /etc/profile
    echo >>/etc/profile
    echo "test -x /client/bin/check_sendfile && /client/bin/check_sendfile" \
         >>/etc/profile
  fi
fi
if [ -f /etc/csh.login ]; then
  if [ "`grep check_sendfile /etc/csh.login`" = "" ]; then
    echo adding check_sendfile to /etc/csh.login
    echo >>/etc/csh.login
    echo "test -x /client/bin/check_sendfile && /client/bin/check_sendfile" \
         >>/etc/csh.login
  fi
fi

if [ ! -d "$SPOOL" ]; then
  echo creating $SPOOL
  mkdir_recursive $SPOOL
fi
if [ ! -d "$SPOOL/LOG" ]; then mkdir $SPOOL/LOG; fi
if [ ! -d "$SPOOL/OUTGOING" ]; then mkdir $SPOOL/OUTGOING; fi
chmod 755  $SPOOL
chmod 700  $SPOOL/LOG
chmod 1777 $SPOOL/OUTGOING

free=`$SENDFILED -f`
minfree=`awk '/minfree =/{print $3}' $CONFIG/sendfile.cf`
if [ $free -le $minfree ]; then
  cat <<EOD
		! ACHTUNG ! 
Es ist nicht genug Platz in $SPOOL !
Entweder muss dem Spool Directory mehr Platz zugewiesen werden oder der Wert
der Option minfree in $CONFIG heruntergesetzt werden.
EOD
fi

echo
echo "Sie koeenen bei Bedarf die folgende Zeile in Ihr System Bootscript aufnehmen:"
echo "    $SERVERDIR/sendfiled -Q"
echo "Dies wird beim Booten einen outgoing spooling sendfile daemon starten,"
echo "der alte liegengebliebene Files im outgoing spool verarbeitet."
echo

if [ "$RESTART" = true ]; then
  echo
  echo "Bitte jetzt den inetd neu starten ( oder einfach neu booten :-) )"
  echo
fi
