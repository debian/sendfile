/*
 * File:	message.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 *
 * History:	1995-08-12  Framstag	initial version
 *              2008-03-12  Framstag	exit on fatalerror flag
 *
 * Header-file for the VMS-like message routine.
 *
 * Copyright � 1995,2008 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */


/* print information, warning and error messages on stderr */
void message(char *, char, const char *);

/* cleanup routine before exit */
void cleanup();
