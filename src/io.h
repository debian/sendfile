/*
 * File:	io.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 *
 * History:	
 *
 *  1995-08-12 Framstag		initial version
 *  1996-04-23 Framstag		added file copying function
 *  2001-01-10 Framstag		added rfopen()
 *  2005-06-02 Framstag		added largefile support
 *  2005-11-13	Framstag        added vsystem()
 *
 * Header-file for the read and write routines of the sendfile package.
 *
 * Copyright � 1995-2005 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */

#ifndef O_LARGEFILE
  #define O_LARGEFILE 0
#endif

#if defined(SOLARIS2)
  #ifndef fileno
    int fileno(FILE *);
  #endif
#endif

#ifdef NEXT
  #include <sys/uio.h>
#endif

#ifndef O_LARGEFILE
  #define O_LARGEFILE 0
#endif

/* read n bytes from network socket */
int readn(int, char *, int);

/* write n bytes to network socket */
int writen(int, char *, int);

/* copy a file */
int fcopy(const char *, const char *, mode_t);

/* where is a program in the path */
char *whereis(char *);

/* open a regular file */
FILE *rfopen(const char *, const char *);

/* mktmpdir - create a new temporary directory */
char *mktmpdir(int);

/* rmtmpdir - delete the temporary directory */
void rmtmpdir(char *);

/* spawn a subprocess and direct output to a file */
int spawn(char **, const char *, mode_t);

/* system() with verbose output */
int vsystem(const char *);

/* popen() with verbose output */
FILE* vpopen(const char *, const char *);

/*  quote a string so it ist shell escape safe */
char* shell_quote(const char *);
