/*
 * File:	bsd.h
 *
 * Author:	Ulli Horlacher (framstag@rus.uni-stuttgart.de)
 * 
 * History:	11 Aug 95   Framstag	initial version
 *              19 Nov 96   Framstag	fix for broken AIX include-files
 *
 * missing prototype definitions in the AIX and Ultrix system include files
 *
 * Copyright � 1995,1996 Ulli Horlacher
 * This file is covered by the GNU General Public License
 */


#ifndef _BSD
  #define _BSD 43
#endif

int socket(int, int, int);
int connect(int, const struct sockaddr *, int);
int shutdown(int, int);
int getpeername(int, struct sockaddr *, int *);
int getsockname(int, struct sockaddr *, int *);
/* int getopt(int, char **, const char *); */
int chown(const char *, uid_t, gid_t);
int chdir(const char *);
time_t time(time_t *);

#if !defined(HAVE_GETTIMEOFDAY)
#if defined(ULTRIX)
  int gettimeofday(struct timeval *, struct timezone *);
#endif

#if defined(AIX)
  int gettimeofday(struct timeval *, void *);
#endif
#endif
